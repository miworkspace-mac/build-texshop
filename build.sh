#!/bin/bash -ex

# CONFIG
prefix="TeXShop"
suffix=""
munki_package_name="TeXShop"
display_name="TeXShop"
category="Scientific"
description="TeXShop is a TeX previewer for Mac OS X. This update contains stability and security fixes for the TeXShop"
url="http://pages.uoregon.edu/koch/texshop/texshop-64/texshop.zip"

# download it (-L: follow redirects)
curl -L -o app.zip -A 'Mozilla/5.0 (Macintosh; Intel Mac OS X 10_8_2) AppleWebKit/536.26.17 (KHTML, like Gecko) Version/6.0.2 Safari/536.26.17' "${url}"

# unzip
unzip app.zip

# make DMG from the inner prefpane
mkdir -p build-root/Applications/TeX
mv TeXShop.app build-root/Applications/TeX

# Obtain version info
version=`/usr/libexec/PlistBuddy -c "Print :CFBundleShortVersionString" build-root/Applications/TeX/TexShop.app/Contents/Info.plist`
#identifier=`/usr/libexec/PlistBuddy -c "Print :CFBundleIdentifier" $app_in_dmg/Contents/Info.plist`
# (cd build-root; pax -rz -f ../pkg/*/Payload)

## Create pkg's
/usr/bin/pkgbuild --root build-root/ --identifier edu.umich.izzy.pkg.$munki_package_name --install-location / --version $version app.pkg

## Find all the appropriate apps, etc, and then turn that into -f's
key_files=`find build-root -name '*.app' -maxdepth 4 | sed 's/ /\\\\ /g; s/^/-f /' | paste -s -d ' ' -`

## Build pkginfo (this is done through an echo to expand key_files)
echo /usr/local/munki/makepkginfo app.pkg ${key_files} | /bin/bash > app.plist

## Fixup and remove "build-root" from file paths
perl -p -i -e 's/build-root//' app.plist

plist=`pwd`/app.plist

# Obtain version info
version=`defaults read "${plist}" version`

# Change path and other details in the plist
defaults write "${plist}" installer_item_location "jenkins/${prefix}-${version}${suffix}.pkg"
defaults write "${plist}" minimum_os_version "10.14.0"
defaults write "${plist}" uninstallable -bool NO
defaults write "${plist}" name "${munki_package_name}"
defaults write "${plist}" display_name "${display_name}"
defaults write "${plist}" description -string "${description}"
defaults write "${plist}" unattended_install -bool TRUE

defaults write "${plist}" update_for -array MacTeX2021 MacTeX2022 MacTex2023 MacTeX2024 MacTeX2025 MacTex2026 MacTeX2027 MacTeX2028 MacTex2029


# Make readable by humans
/usr/bin/plutil -convert xml1 "${plist}"
chmod a+r "${plist}"

# Change filenames to suit
mv app.pkg   ${prefix}-${version}${suffix}.pkg
mv app.plist ${prefix}-${version}${suffix}.plist
