#!/bin/bash -ex

url="http://pages.uoregon.edu/koch/texshop/texshop-64/texshop.zip"

# download it (-L: follow redirects)
curl -L -o app.zip -A 'Mozilla/5.0 (Macintosh; Intel Mac OS X 10_8_2) AppleWebKit/536.26.17 (KHTML, like Gecko) Version/6.0.2 Safari/536.26.17' "${url}"

# unzip
unzip app.zip

# Obtain version info from APP


VERSION=`/usr/libexec/PlistBuddy -c "Print :CFBundleShortVersionString" Texshop.app/Contents/Info.plist`

if [ "x${VERSION}" != "x" ]; then
	echo "${VERSION}"
fi

rm -rf TexShop.app
rm -rf app.zip
rm -rf __MACOSX


if [ "x${VERSION}" != "x" ]; then
    echo version: "${VERSION}"
    echo "${VERSION}" > current-version
fi

# Update to handle distributed builds
if cmp current-version old-version; then
    # Files are identical, exit 1 to NOT trigger the build job
    exit 1
else
    # Files are different - copy marker, exit 0 to trigger build job
    cp current-version old-version
    exit 0
fi